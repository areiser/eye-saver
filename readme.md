# Eye Saver
A simple utility reminding you to look at something distant every 20 minutes.

This helps save your eyes from getting worse over time and can also prevent headaches after using 
your computer for a while.
## Starting automatically (Linux)
To automatically start this tool, use the provided `eye-saver.service` file in the conf directory 
and place it in the `/usr/lib/systemd/user` directory.

To enable autostart, execute

`systemctl --user enable eye-saver.service`

The executable should be in the place provided.
Either restart your computer or execute

`systemctl daemon-reload`

`systemctl --user start eye-saver.service`