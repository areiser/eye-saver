extern crate notify_rust;

use std::{thread, time};

use notify_rust::Notification;

#[cfg(all(unix, not(target_os = "windows")))]
fn main() {
    let twenty_minutes = time::Duration::from_secs(1200);

    let mut paused = false;
    while !paused {
        thread::sleep(twenty_minutes);

        create_notification()
            .show()
            .expect("Should be able to show notification")
            .wait_for_action(|action| match action {
                "pause" => paused = true,
                _ => ()
            });
    }
}

fn create_notification() -> Notification {
    Notification::new()
        .summary("Eye Saver")
        .body("Look out the window for 20 seconds to save your eyesight.")
        .icon("face-glasses")
        .action("pause", "Pause until reboot").to_owned()
}

#[cfg(target_os = "windows")]
fn main() {
    let twenty_minutes = time::Duration::from_secs(1200);

    loop {
        thread::sleep(twenty_minutes);

        create_notification()
            .show()
            .expect("Should be able to show notification");
    }
}